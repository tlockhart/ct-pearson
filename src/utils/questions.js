export const questions = [
    {
        id: 1,
        question: "What color is the sky?",
        choices: {
            1: "blue",
            2: "green",
            3: "yellow",
            4: "white",
            5: "orange"
        }
    },
    {
        id: 2,
        question: "What color is the sun?",
        choices: {
            1: "blue",
            2: "green",
            3: "yellow",
            4: "white",
            5: "orange"
        }
    },
    {
        id: 3,
        question: "What color is the moon?",
        choices: {
            1: "blue",
            2: "green",
            3: "yellow",
            4: "white",
            5: "orange"
        }
    },
    {
        id: 4,
        question: "What color is the grass?",
        choices: {
            1: "blue",
            2: "green",
            3: "yellow",
            4: "white",
            5: "orange"
        }
    },
    {
        id: 5,
        question: "What color is the flame?",
        choices: {
            1: "blue",
            2: "green",
            3: "yellow",
            4: "white",
            5: "orange"
        }
    },
];