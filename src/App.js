// import logo from './logo.svg';
// import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import Details from "./containers/Details";
//Import containers
import Home from "./containers/Intro";
import Summary from "./containers/Summary";
import Quiz from "./containers/Quiz";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/summary" component={Summary} />
          <Route exact path="/quiz/:id" component={Quiz} />
        </Switch>
      </Router>
    </>
    );
}

export default App;
