import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdb-react-ui-kit';
import { useParams, useHistory } from 'react-router-dom';
import { urlParser } from "../../utils/url-parser";
import React, { useEffect, useState } from 'react';
//components
import {Question} from '../../component/Question';

const Quiz = () => {
    // get id from params
    const params = useParams();
    const baseUrl = urlParser();
    let { id } = params;
    const curQuestNum = parseInt(id);
    const startIdx = 1;
    const endIdx = 5;

    console.log(`curQuestNum: ${curQuestNum}`);
    let history = useHistory();

    const [pageNum, setPageNum] = useState(curQuestNum);

    let transition = (event) => {
        event.preventDefault();
        const { name } = event.target;
        console.log(`name: ${name}`);
        if (name === "next") {
            setPageNum((prevState) => {
                console.log("next:", prevState);
                const newNum = (prevState < endIdx) ? (prevState + 1) : endIdx;
                return newNum;
            });
        }
        else if (name === "prev") {
            setPageNum((prevState) => {
                console.log("prev:", prevState);
                const newNum = (prevState > startIdx) ? (prevState - 1) : startIdx;
                return newNum;
            });
        }
        
    };
useEffect(()=>{
    const curUrl = `${baseUrl}/${pageNum}`;
    history.push(curUrl);
},[pageNum]);
    return (
        <>
            <MDBContainer>
                <MDBRow>
                    <Question
                    id={pageNum}/>
                </MDBRow>
                <MDBRow>
                    <MDBCol>
                        <h1 className="text-center">Quiz</h1>
                    </MDBCol>
                </MDBRow>
                <MDBRow className='d-flex justify-content-center'>
                    {
                        <p>THIS IS THE Quiz</p>
                    }
                </MDBRow>
                <MDBRow>
                    <MDBCol size="8">
                        <MDBBtn
                            name="prev"
                            type="button"
                            color="primary"
                            className="btn btn-radius"
                            data-dismiss="modal"
                            onClick={(event) => { transition(event) }}
                        >
                            prev:{pageNum}
                        </MDBBtn>
                        <MDBBtn
                            name="next"
                            type="button"
                            color="primary"
                            className="btn btn-radius"
                            data-dismiss="modal"
                            onClick={(event) => { transition(event) }}
                        >
                            next:{pageNum}
                        </MDBBtn>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </>
    );
};

export default Quiz;