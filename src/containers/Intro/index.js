import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit';

const Intro = () => {
    return (
        <>
            <MDBContainer>
                <MDBRow>
                    <MDBCol>
                        <h1 className="text-center">Businesses</h1>
                    </MDBCol>
                </MDBRow>
                <MDBRow className='d-flex justify-content-center'>
                    {
                        <p>THIS IS THE INTRODUCTION</p>
                    }
                </MDBRow>
            </MDBContainer>
        </>
    );
};

export default Intro;