import { MDBContainer, MDBRow, MDBCol } from 'mdb-react-ui-kit';

const Summary = () => {
    return (
        <>
            <MDBContainer>
                <MDBRow>
                    <MDBCol>
                        <h1 className="text-center">Summary</h1>
                    </MDBCol>
                </MDBRow>
                <MDBRow className='d-flex justify-content-center'>
                    {
                        <p>THIS IS THE Summary</p>
                    }
                </MDBRow>
            </MDBContainer>
        </>
    );
};

export default Summary;