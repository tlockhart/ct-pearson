import React from 'react';
// import './style.css';
import { questions } from '../../utils/questions';
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBTypography } from 'mdb-react-ui-kit';
import {RadioCheckGroup} from '@pearson-ux/pearson-web-components';


export let Question = ({ id }) => {
    // let data = props.data.text;
    // let checked = props.data.completed;
    // let id = props.data.id;
    console.log(`id22: ${id}`);
    const data = questions.find((item) => item.id === id);
    return (
        <>
            <MDBContainer>
                <MDBRow>
                    <MDBCol>
                        <MDBTypography tag='h1' className="text-center">
                            {data.question}
                        </MDBTypography>
                    </MDBCol>
                </MDBRow>
                <MDBRow>
                    <MDBCol>
                        <RadioCheckGroup 
                            label="Label" groupName="groupOne"
                        >
                            <input name="first option" />
                            <input name="second option" />
                            <input name="third option" />
                        </RadioCheckGroup>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
            {/* <div className="container-fluid">
                <div className="row mx-0">
                    <div className="column to-do-item"> */}

            {/* <input 
                            type= "checkbox" 
                            name= {data} 
                            value= {data} 
                            checked ={checked} 
                            onChange = {()=>{props.changeHandler(id)}}
                        />
                        <p className = {!checked? "no-strike": "striked"}>{data}</p> */}

            {/* </div>{ }
                </div >
            </div > */}
        </>
    )
};

// export default Question;